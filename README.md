# Heartbeat-Dashboard

The Heartbeat-Dashboard visualizes all registered [Heartbeat-Clients](https://github.com/LuisNaldo7/heartbeat-client) and their states by connecting to the [Heartbeat-Server's](https://github.com/LuisNaldo7/heartbeat-server) REST API.

## Components

[Heartbeat-Client](https://github.com/LuisNaldo7/heartbeat-client)

[Heartbeat-Server](https://github.com/LuisNaldo7/heartbeat-server)

[Heartbeat-Alert](https://github.com/LuisNaldo7/heartbeat-alert)

[Heartbeat-Dashboard](https://github.com/LuisNaldo7/heartbeat-dashboard)

![Diagram](https://github.com/LuisNaldo7/heartbeat-local-dev-env/blob/main/docs/components.png?raw=true)

---

## Run

Copy .env.example to .env and adjust values.

install dependencies

```bash
npm install
```

start app

```bash
npm start
```

A full integration can be set up using the [Local Development Environment](https://github.com/LuisNaldo7/heartbeat-local-dev-env).

## Run in Docker

build image

```bash
docker build --build-arg HEARTBEAT_SERVER="http://localhost:3000" -t luisnaldo7/heartbeat-dashboard:latest -f docker/Dockerfile .
```

execute image

```bash
docker run -d -p 3001:3001 --rm --name heartbeat-dashboard luisnaldo7/heartbeat-dashboard:latest
```

run container on boot

```bash
docker run -d -p 3001:3001 --restart always --name heartbeat-dashboard luisnaldo7/heartbeat-dashboard:latest
```
